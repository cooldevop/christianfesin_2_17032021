## Parcours formation Développeur Web - Openclassrooms

Projet 2 - 90 heures

# Transformez une maquette en site web

    Démarrez sur HTML et CSS en intégrant la page Web de Reservia, une
    plateforme de réservation de vacances.

Compétences cibles
- Implémenter une interface responsive
- Mettre en place son environnement Front-End
- Intégrer du contenu conformément à une maquette
- Utiliser un système de gestion de versions pour le suivi du projet et son hébergement

A suivre ...
